# Examples for Docker for Developers _sdg Presents_ talk

Learn how step by step the anatomy of a container, building a container, using containers in workflow & monitoring/optimization

## Building from the ground up, our first Dockerfile:

Save this snippet as `Dockerfile`:
```yaml
FROM alpine:latest
ENV name Docker Developers
ENTRYPOINT echo "Hello, $name!"
```

In Bash or Powershell:
```bash
docker build .
```

## dotnet
To test out dotnet core (requires installing dotnetcore locally for build):
```bash
git clone git@gitlab.com:Landro/docker-4-developers.git
```
then
```bash
cd dotnet
dotnet publish -c Release -o out
docker build .
docker run -p 4444:80 ${HASH_OF_CONTAINER}
```
then you should be able to hit http://localhost:4444/api/values

## Java
To run through a MAven build in a docker container and use multi-stage builds:
```bash
git clone git@gitlab.com:Landro/docker-4-developers.git
```
then
```bash
cd java
docker build .
docker run ${HASH_OF_CONTAINER}
```

## Node & PostgresSQL

To use this node app with PostgresSQL in a Docker container:
Run PostgresSQL in Docker container
```bash
docker-compose up
```
Install & Run
```bash
npm install
npm start
```

## Portainer

```bash
docker volume create portainer_data​

docker run -d -p 8000:8000 -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```


